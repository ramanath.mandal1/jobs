import { MailerService } from '@nestjs-modules/mailer';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class MailService {
  constructor(
    private mailerService: MailerService,
    private configService: ConfigService,
  ) {}

  async sendVerificationEmail(email: string, name: string, token: string) {
    const url = `${this.configService.get('VERIFY_EMAIL_URL')}${token}`;

    await this.mailerService.sendMail({
      to: email,
      subject: 'Welcome to JoB Finder! Confirm your Email',
      template: './signup',
      context: {
        name: name,
        url,
      },
    });
  }

  async sendPasswordResetSuccessfulEmail(email: string) {
    await this.mailerService.sendMail({
      to: email,
      subject: 'Password reset was successful',
      template: './password-reset-successful',
    });
  }

  async sendForgotPasswordEmail(email: string, token: string) {
    const url = `${this.configService.get('RESET_PASSWORD_URL')}${token}`;

    await this.mailerService.sendMail({
      to: email,
      subject: 'Reset your password',
      template: './forgot-password',
      context: {
        url,
      },
    });
  }
}
