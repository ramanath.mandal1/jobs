import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema({ timestamps: true })
export class Type extends Document {
  @Prop({ required: true, unique: true })
  name: string;
}

export const TypeSchema = SchemaFactory.createForClass(Type);
