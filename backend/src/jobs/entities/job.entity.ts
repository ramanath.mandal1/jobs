import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { Category, Location, PostedBy, Type, Skill } from '../types/job.type';

@Schema({ timestamps: true })
export class Job extends Document {
  @Prop({ required: true })
  title: string;

  @Prop({ required: true })
  company: string;

  @Prop({ required: true, type: Object })
  type: Type;

  @Prop()
  category: Category;

  @Prop({ required: true })
  description: string;

  @Prop()
  locations: Location[];

  @Prop()
  skills: Skill[];

  @Prop({ required: true })
  experience: string;

  @Prop({ required: true })
  lastApplicationDate: Date;

  @Prop({ default: true })
  isActive: boolean;

  @Prop({ required: true })
  postedBy: PostedBy;
}

const JobSchema = SchemaFactory.createForClass(Job);

export { JobSchema };
