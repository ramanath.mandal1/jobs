import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectConnection, InjectModel } from '@nestjs/mongoose';
import { ClientSession, Connection, Model } from 'mongoose';
import { CreateSkillDto } from './dto/create-skill.dto';
import { UpdateSkillDto } from './dto/update-skill.dto';
import { Job } from './entities/job.entity';
import { Skill } from './entities/skill.entity';
import { User } from '../users/entities/user.entity';

@Injectable()
export class SkillsService {
  constructor(
    @InjectModel(Skill.name) private readonly skillModel: Model<Skill>,
    @InjectModel(Job.name) private readonly jobModel: Model<Job>,
    @InjectModel(User.name) private readonly userModel: Model<User>,
    @InjectConnection() private readonly connection: Connection,
  ) {}

  create(createSkillDto: CreateSkillDto): { message: string } {
    const skill = new this.skillModel(createSkillDto);
    skill.save();

    return { message: 'Skill created successfully' };
  }

  async findAll(): Promise<{ message: string; result: Skill[] }> {
    const skills = await this.skillModel.find().exec();
    return { message: 'Skills retrieved successfully', result: skills };
  }

  async findOne(id: string): Promise<{ message: string; result: Skill }> {
    const skill = await this.skillModel.findOne({ _id: id }).exec();
    if (!skill) {
      throw new NotFoundException(`Skill #${id} not found`);
    }

    return { message: 'Skill retrieved successfully', result: skill };
  }

  async update(
    id: string,
    updateSkillDto: UpdateSkillDto,
  ): Promise<{ message: string; result: string }> {
    const newSkill = updateSkillDto.skill;
    const session = await this.connection.startSession();
    session.startTransaction();

    try {
      // will return old skill but skill will be updated since { new: false } by default
      const { skill } = await this.skillModel
        .findByIdAndUpdate(id, updateSkillDto)
        .session(session);

      // will update "skill" to "newSkill" in the skills array field to all the matched documents.
      await this.jobModel
        .updateMany({ skills: skill }, { $set: { 'skills.$': newSkill } })
        .session(session);

      // will update "skill" to "newSkill" in the skills array field to all the matched documents.
      await this.updateSkill(session, skill, newSkill);

      await session.commitTransaction();
    } catch (err) {
      await session.abortTransaction();
      console.log(err);
    } finally {
      session.endSession();
    }

    return { message: 'Skill successfully updated', result: newSkill };
  }

  async remove(id: string): Promise<{ message: string }> {
    const session = await this.connection.startSession();
    session.startTransaction();

    try {
      // will remove the skill
      const { skill } = await this.skillModel
        .findByIdAndRemove(id)
        .session(session);

      // will update "skill" to "" in the skills array field to all the matched documents.
      await this.jobModel
        .updateMany({}, { $pull: { skills: skill } })
        .session(session);

      // will remove "skill" to "" in the skills array field to all the matched documents.
      await this.removeSkill(session, skill);

      await session.commitTransaction();
    } catch (err) {
      await session.abortTransaction();
    } finally {
      session.endSession();
    }

    return { message: 'Skill successfully removed' };
  }

  async updateSkill(
    session: ClientSession | null,
    oldSkill: string,
    newSkill: string,
  ) {
    return await this.userModel
      .updateMany({ skills: oldSkill }, { $set: { 'skills.$': newSkill } })
      .session(session);
  }

  async removeSkill(session: ClientSession | null, skill: string) {
    return await this.userModel
      .updateMany({}, { $pull: { skills: skill } })
      .session(session);
  }
}
