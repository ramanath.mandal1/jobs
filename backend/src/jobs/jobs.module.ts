import { User, UserSchema } from './../users/entities/user.entity';
import { Module } from '@nestjs/common';
import { JobsService } from './jobs.service';
import { JobsController } from './jobs.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Job, JobSchema } from './entities/job.entity';
import { Category, CategorySchema } from './entities/category.entity';
import { Location, LocationSchema } from './entities/location.entity';
import { Skill, SkillSchema } from './entities/skill.entity';
import { Type, TypeSchema } from './entities/type.entity';
import { SkillsController } from './skills.controller';
import { SkillsService } from './skills.service';
import { ConfigService } from '@nestjs/config';
import { LocationsController } from './locations.controller';
import { LocationsService } from './locations.service';
import { CategoriesController } from './categories.controller';
import { CategoriesService } from './categories.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: User.name, schema: UserSchema },
      { name: Type.name, schema: TypeSchema },
      { name: Skill.name, schema: SkillSchema },
      { name: Location.name, schema: LocationSchema },
      { name: Category.name, schema: CategorySchema },
      { name: Job.name, schema: JobSchema },
    ]),
  ],
  controllers: [
    JobsController,
    SkillsController,
    LocationsController,
    CategoriesController,
  ],
  providers: [
    JobsService,
    SkillsService,
    LocationsService,
    CategoriesService,
    ConfigService,
  ],
})
export class JobsModule {}
