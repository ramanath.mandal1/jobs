export class PostedBy {
  id: string;
  name: string;
}

export type Type = string;

export type Category = string;

export type Location = string;

export type Skill = string;
