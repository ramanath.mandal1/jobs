import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectConnection, InjectModel } from '@nestjs/mongoose';
import { ClientSession, Connection, Model } from 'mongoose';
import { CreateCategoryDto } from './dto/create-category.dto';
import { UpdateCategoryDto } from './dto/update-category.dto';
import { Category } from './entities/category.entity';
import { Job } from './entities/job.entity';

@Injectable()
export class CategoriesService {
  constructor(
    @InjectModel(Category.name) private readonly categoryModel: Model<Category>,
    @InjectModel(Job.name) private readonly jobModel: Model<Job>,
    @InjectConnection() private readonly connection: Connection,
  ) {}

  create(createCategoryDto: CreateCategoryDto): { message: string } {
    const category = new this.categoryModel(createCategoryDto);
    category.save();

    return { message: 'Category created successfully' };
  }

  async findAll(): Promise<{ message: string; result: Category[] }> {
    const categories = await this.categoryModel.find().exec();
    return { message: 'Categories retrieved successfully', result: categories };
  }

  async findOne(id: string): Promise<{ message: string; result: Category }> {
    const category = await this.categoryModel.findOne({ _id: id }).exec();
    if (!category) {
      throw new NotFoundException(`Category #${id} not found`);
    }

    return { message: 'Category retrieved successfully', result: category };
  }

  async update(
    id: string,
    updateCategoryDto: UpdateCategoryDto,
  ): Promise<{ message: string; result: string }> {
    const newCategory = updateCategoryDto.name;
    const session = await this.connection.startSession();
    session.startTransaction();

    try {
      // will return old category but category will be updated since { new: false } by default
      const { name } = await this.categoryModel
        .findByIdAndUpdate(id, updateCategoryDto)
        .session(session);

      // will update "category" to "newCategory" in the category field to all the matched documents.
      await this.jobModel
        .updateMany({ category: name }, { $set: { 'category.$': newCategory } })
        .session(session);

      await session.commitTransaction();
    } catch (err) {
      await session.abortTransaction();
      console.log(err);
    } finally {
      session.endSession();
    }

    return { message: 'Category successfully updated', result: newCategory };
  }

  async remove(id: string): Promise<{ message: string }> {
    const session = await this.connection.startSession();
    session.startTransaction();

    try {
      // will remove the category
      const { name } = await this.categoryModel
        .findByIdAndRemove(id)
        .session(session);

      // will update "category" to "" in the category field to all the matched documents.
      await this.jobModel
        .updateMany({}, { $pull: { category: name } })
        .session(session);

      await session.commitTransaction();
    } catch (err) {
      await session.abortTransaction();
    } finally {
      session.endSession();
    }

    return { message: 'Category successfully removed' };
  }
}
