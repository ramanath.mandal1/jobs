import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectConnection, InjectModel } from '@nestjs/mongoose';
import { Connection, Model } from 'mongoose';
import { CreateLocationDto } from './dto/create-location.dto';
import { UpdateLocationDto } from './dto/update-location.dto';
import { Job } from './entities/job.entity';
import { Location } from './entities/location.entity';

@Injectable()
export class LocationsService {
  constructor(
    @InjectModel(Location.name) private readonly locationModel: Model<Location>,
    @InjectModel(Job.name) private readonly jobModel: Model<Job>,
    @InjectConnection() private readonly connection: Connection,
  ) {}

  create(createLocationDto: CreateLocationDto): { message: string } {
    const location = new this.locationModel(createLocationDto);
    location.save();

    return { message: 'Location created successfully' };
  }

  async findAll(): Promise<{ message: string; result: Location[] }> {
    const locations = await this.locationModel.find().exec();
    return { message: 'Locations retrieved successfully', result: locations };
  }

  async findOne(id: string): Promise<{ message: string; result: Location }> {
    const location = await this.locationModel.findOne({ _id: id }).exec();
    if (!location) {
      throw new NotFoundException(`Location #${id} not found`);
    }

    return { message: 'Location retrieved successfully', result: location };
  }

  async update(
    id: string,
    updateLocationDto: UpdateLocationDto,
  ): Promise<{ message: string; result: string }> {
    const newLocation = updateLocationDto.location;
    const session = await this.connection.startSession();
    session.startTransaction();

    try {
      // will return old location but location will be updated since { new: false } by default
      const { location } = await this.locationModel
        .findByIdAndUpdate(id, updateLocationDto)
        .session(session);

      // will update "location" to "newLocation" in the locations array field to all the matched documents.
      await this.jobModel
        .updateMany(
          { locations: location },
          { $set: { 'locations.$': newLocation } },
        )
        .session(session);

      await session.commitTransaction();
    } catch (err) {
      await session.abortTransaction();
      console.log(err);
    } finally {
      session.endSession();
    }

    return { message: 'Location successfully updated', result: newLocation };
  }

  async remove(id: string): Promise<{ message: string }> {
    const session = await this.connection.startSession();
    session.startTransaction();

    try {
      // will remove the location
      const { location } = await this.locationModel
        .findByIdAndRemove(id)
        .session(session);

      // will update "location" to "" in the locations array field to all the matched documents.
      await this.jobModel
        .updateMany({}, { $pull: { locations: location } })
        .session(session);

      await session.commitTransaction();
    } catch (err) {
      await session.abortTransaction();
    } finally {
      session.endSession();
    }

    return { message: 'Location successfully removed' };
  }
}
