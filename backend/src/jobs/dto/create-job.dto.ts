import { Type } from 'class-transformer';
import {
  IsBoolean,
  IsDateString,
  IsDefined,
  IsNotEmpty,
  IsNotEmptyObject,
  IsObject,
  IsString,
  ValidateNested,
} from 'class-validator';
import { Category, PostedBy, Skill, Type as JobType } from '../types/job.type';

export class CreateJobDto {
  @IsNotEmpty()
  @IsString()
  title: string;

  @IsNotEmpty()
  @IsString()
  company: string;

  @IsNotEmpty()
  @IsString()
  type: JobType;

  @IsNotEmpty()
  @IsString()
  category: Category;

  @IsNotEmpty()
  @IsString()
  description: string;

  @IsNotEmpty()
  @IsString({ each: true })
  locations: Location[];

  @IsNotEmpty()
  @IsString({ each: true })
  skills: Skill[];

  @IsNotEmpty()
  @IsString()
  experience: string;

  @IsNotEmpty()
  @IsDateString()
  lastApplicationDate: Date;

  @IsBoolean()
  isActive: boolean;

  @IsDefined()
  @IsNotEmptyObject()
  @IsObject()
  @ValidateNested()
  @Type(() => PostedBy)
  postedBy: PostedBy;
}
