import { IsNotEmpty, IsString } from 'class-validator';
import { Category } from '../types/job.type';

export class CreateCategoryDto {
  @IsNotEmpty()
  @IsString()
  name: Category;
}
