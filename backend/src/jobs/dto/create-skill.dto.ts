import { IsNotEmpty, IsString } from 'class-validator';
import { Skill } from '../types/job.type';

export class CreateSkillDto {
  @IsNotEmpty()
  @IsString()
  skill: Skill;
}
