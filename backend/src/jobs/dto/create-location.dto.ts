import { IsNotEmpty, IsString } from 'class-validator';
import { Location } from '../types/job.type';

export class CreateLocationDto {
  @IsNotEmpty()
  @IsString()
  location: Location;
}
