import { BadRequestException, ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as cookieParser from 'cookie-parser';
// import { AllExceptionsFilter } from './common/filters/all-exceptions-filter';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.setGlobalPrefix('v1/api');
  app.use(cookieParser());
  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      transform: true,
      transformOptions: {
        enableImplicitConversion: true,
      },
      forbidNonWhitelisted: true,
      exceptionFactory: (errors) => {
        const errorMessages = {};
        const errorsBag = [];
        errors.forEach((error) => {
          errorMessages[error.property] = Object.values(error.constraints);
        });
        errorsBag.push(errorMessages);
        return new BadRequestException(errorsBag);
      },
    }),
  );
  app.enableCors({
    credentials: true,
    origin: process.env.FRONTEND_URL,
  });
  // app.useGlobalFilters(new AllExceptionsFilter());
  await app.listen(3000);
}
bootstrap();
