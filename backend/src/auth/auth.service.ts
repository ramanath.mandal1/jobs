import { User } from './../users/entities/user.entity';
import { ConfigService } from '@nestjs/config';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { JwtService } from '@nestjs/jwt';
import { Request, Response } from 'express';
import { CreateUserDto } from '../users/dto/create-user.dto';
import { UsersService } from '../users/users.service';
import { LoginUserDto } from './dto/login-user.dto';
import { UserRegisteredEvent } from './events/user-registered.event';
import { JwtPayload } from './jwt-payload';
import { UserForgotPasswordEvent } from './events/user-forgot-password.event';
import { UserPasswordResetSuccessfulEvent } from './events/user-password-reset-successful.event';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
    private eventEmitter: EventEmitter2,
    private configService: ConfigService,
  ) {}

  async validateUser(payload: JwtPayload): Promise<User> {
    const user = await this.usersService.findByPayload(payload);
    if (!user) {
      throw new HttpException('Invalid token', HttpStatus.UNAUTHORIZED);
    }
    return user;
  }

  async register(userDto: CreateUserDto, verificationRequired: boolean) {
    const registeredUser = await this.usersService.create(
      userDto,
      verificationRequired,
    );

    if (verificationRequired) {
      const userRegisteredEvent = new UserRegisteredEvent(
        registeredUser.name,
        registeredUser.email,
        registeredUser.verificationUrlToken,
      );
      this.eventEmitter.emit('user.registered', userRegisteredEvent);
    }

    return {
      message:
        'Registration was successful! Please verify your email to get started.',
    };
  }

  async login(
    loginUserDto: LoginUserDto,
    request: Request,
    response: Response,
  ) {
    const user = await this.usersService.findUserByCredentials(loginUserDto);
    const token = this._createToken(user, request, response);

    return {
      message: 'You have logged in successfully.',
      result: {
        email: user.email,
        ...token,
      },
    };
  }

  logout(response: Response) {
    response.cookie('jwt', 'logOut', {
      expires: new Date(Date.now() + 10 * 1000),
      httpOnly: true,
    });

    return { message: 'You have logged out successfully' };
  }

  async forgotPassword(email: string) {
    const user = await this.usersService.forgotPassword(email);
    const userForgotPasswordEvent = new UserForgotPasswordEvent(
      user.email,
      user.resetUrlToken,
    );
    this.eventEmitter.emit('user.forgot.password', userForgotPasswordEvent);

    return {
      message: 'A email has been sent to you for resetting the password',
    };
  }

  async resetPassword(token: string, password: string) {
    const user = await this.usersService.resetPassword(token, password);
    const userPasswordResetSuccessfulEvent =
      new UserPasswordResetSuccessfulEvent(user.email);
    this.eventEmitter.emit(
      'user.password.reset.successful',
      userPasswordResetSuccessfulEvent,
    );

    return { message: 'You have successfully reset the password' };
  }

  async verifyEmail(token: string, request: Request, response: Response) {
    const user = await this.usersService.verifyEmail(token);
    const accessToken = this._createToken(user.email, request, response);

    return {
      message: 'Email verification was successful.',
      result: {
        email: user.email,
        ...accessToken,
      },
    };
  }

  async resendEmail(email: string) {
    const registeredUser = await this.usersService.resendEmail(email);
    const userRegisteredEvent = new UserRegisteredEvent(
      registeredUser.name,
      registeredUser.email,
      registeredUser.verificationUrlToken,
    );
    this.eventEmitter.emit('user.registered', userRegisteredEvent);

    return {
      message:
        'A verification email was successfully sent to you. Please verify.',
    };
  }

  private _createToken(
    { email }: JwtPayload,
    request: Request,
    response: Response,
  ): any {
    const accessToken = this.jwtService.sign({ email });
    response.cookie('jwt', accessToken, {
      expires: new Date(Date.now() + +this.configService.get('JWT_EXPIRES_IN')), // milliseconds,
      sameSite: 'strict',
      httpOnly: true,
      domain: this.configService.get('FRONTEND_DOMAIN'),
      secure:
        request.secure || request.headers['x-forwarded-proto'] === 'https',
    });
    return {
      expiresIn: +this.configService.get('JWT_EXPIRES_IN'),
      accessToken,
    };
  }
}
