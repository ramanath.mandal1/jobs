import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { MailService } from '../mail/mail.service';
import { UsersModule } from '../users/users.module';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { JwtStrategy } from './jwt.strategy';
import { UserForgotPasswordListener } from './listeners/user-forgot-password.listener';
import { UserPasswordResetSuccessfulListener } from './listeners/user-password-reset-successful.listner';
import { UserRegisteredListener } from './listeners/user-registered.listener';

@Module({
  imports: [
    UsersModule,
    PassportModule.register({
      defaultStrategy: 'jwt',
      property: 'user',
      session: false,
    }),
    JwtModule.registerAsync({
      useFactory: () => ({
        secret: process.env.JWT_SECRET_KEY,
        signOptions: {
          expiresIn: +process.env.JWT_EXPIRES_IN,
        },
      }),
    }),
  ],
  providers: [
    AuthService,
    JwtStrategy,
    UserRegisteredListener,
    UserForgotPasswordListener,
    UserPasswordResetSuccessfulListener,
    MailService,
    ConfigService,
  ],
  exports: [PassportModule, JwtModule],
  controllers: [AuthController],
})
export class AuthModule {}
