import { MailService } from './../../mail/mail.service';
import { Injectable } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';
import { UserRegisteredEvent } from '../events/user-registered.event';

@Injectable()
export class UserRegisteredListener {
  constructor(private readonly mailService: MailService) {}

  @OnEvent('user.registered')
  async handleUserRegisteredEvent(event: UserRegisteredEvent) {
    // handle and process "UserRegistered" event
    await this.mailService.sendVerificationEmail(
      event.email,
      event.name,
      event.token,
    );
  }
}
