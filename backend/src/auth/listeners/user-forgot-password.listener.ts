import { MailService } from '../../mail/mail.service';
import { Injectable } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';
import { UserForgotPasswordEvent } from '../events/user-forgot-password.event';

@Injectable()
export class UserForgotPasswordListener {
  constructor(private readonly mailService: MailService) {}

  @OnEvent('user.forgot.password')
  async handleUserForgotPasswordEvent(event: UserForgotPasswordEvent) {
    // handle and process "UserForgotPassword" event
    await this.mailService.sendForgotPasswordEmail(event.email, event.token);
  }
}
