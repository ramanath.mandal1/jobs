import { MailService } from '../../mail/mail.service';
import { Injectable } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';
import { UserPasswordResetSuccessfulEvent } from '../events/user-password-reset-successful.event';

@Injectable()
export class UserPasswordResetSuccessfulListener {
  constructor(private readonly mailService: MailService) {}

  @OnEvent('user.password.reset.successful')
  async handleUserPasswordResetSuccessfulEvent(
    event: UserPasswordResetSuccessfulEvent,
  ) {
    // handle and process "UserPasswordResetSuccessful" event
    await this.mailService.sendPasswordResetSuccessfulEmail(event.email);
  }
}
