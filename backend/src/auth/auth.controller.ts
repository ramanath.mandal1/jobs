import { CreateUserDto } from '../users/dto/create-user.dto';
import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Post,
  Req,
  Res,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import { LoginUserDto } from './dto/login-user.dto';
import { Request, Response } from 'express';
import { ForgotPasswordDto } from './dto/forgot-password.dto';
import { ResetPasswordDto } from './dto/reset-password.dto';
import { Public } from '../common/decorators/public.decorator';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Public()
  @Post('/register')
  register(@Body() createUserDto: CreateUserDto) {
    return this.authService.register(createUserDto, true);
  }

  @Public()
  @Post('/login')
  login(
    @Body() loginUserDto: LoginUserDto,
    @Req() request: Request,
    @Res({ passthrough: true }) response: Response,
  ) {
    return this.authService.login(loginUserDto, request, response);
  }

  @Get('/logout')
  logout(@Res({ passthrough: true }) response: Response) {
    return this.authService.logout(response);
  }

  @Public()
  @Post('/forgot-password')
  forgotPassword(@Body() { email }: ForgotPasswordDto) {
    return this.authService.forgotPassword(email);
  }

  @Public()
  @Patch('/reset-password/:token')
  resetPassword(
    @Param('token') token: string,
    @Body() { password }: ResetPasswordDto,
  ) {
    return this.authService.resetPassword(token, password);
  }

  @Public()
  @Patch('/verify-email/:token')
  verifyEmail(
    @Param('token') token: string,
    @Req() request: Request,
    @Res({ passthrough: true }) response: Response,
  ) {
    return this.authService.verifyEmail(token, request, response);
  }

  @Public()
  @Post('/resend-verification-email')
  resendVerificationEmail(@Body() { email }: ForgotPasswordDto) {
    return this.authService.resendEmail(email);
  }
}
