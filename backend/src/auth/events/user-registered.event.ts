export class UserRegisteredEvent {
  public name: string;
  public token: string;
  public email: string;

  constructor(name: string, email: string, verificationUrlToken: string) {
    this.name = name;
    this.email = email;
    this.token = verificationUrlToken;
  }
}
