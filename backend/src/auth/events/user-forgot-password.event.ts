export class UserForgotPasswordEvent {
  constructor(public readonly email: string, public readonly token: string) {}
}
