export class UserPasswordResetSuccessfulEvent {
  constructor(public readonly email: string) {}
}
