import { ConfigService } from '@nestjs/config';
import { LoginUserDto } from './../auth/dto/login-user.dto';
import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { JwtPayload } from '../auth/jwt-payload';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';
import * as bcrypt from 'bcrypt';
import * as crypto from 'crypto';
import * as moment from 'moment';

@Injectable()
export class UsersService {
  constructor(
    @InjectModel(User.name) private readonly userModel: Model<User>,
    private readonly configService: ConfigService,
  ) {}

  async create(
    createUserDto: CreateUserDto,
    verificationRequired = false,
  ): Promise<any> {
    const user = new this.userModel(createUserDto);

    if (verificationRequired) {
      return this.verificationUrlToken(user);
    }

    return user.save();
  }

  async findAll() {
    const users = await this.userModel.find().exec();
    return { message: 'Users retrieved successfully', result: users };
  }

  async findOne(id: string) {
    const user = await this.userModel.findOne({ _id: id }).exec();
    if (!user) {
      throw new NotFoundException(`User #${id} not found`);
    }

    return { message: 'User retrieved successfully', result: user };
  }

  async update(id: string, updateUserDto: UpdateUserDto) {
    const existingUser = await this.userModel
      .findOneAndUpdate({ _id: id }, { $set: updateUserDto }, { new: true })
      .exec();

    if (!existingUser) {
      throw new NotFoundException(`User #${id} not found`);
    }

    return { message: 'User info updated successfully', result: existingUser };
  }

  async remove(id: string) {
    const user = await this.userModel.findOne({ _id: id });
    if (!user) {
      throw new NotFoundException(`User #${id} not found`);
    }

    user.remove();
    return { message: 'User removed  successfully' };
  }

  async findByPayload({ email }: JwtPayload): Promise<User> {
    return await this.userModel.findOne({ email: email });
  }

  async findUserByCredentials({
    email,
    password,
  }: LoginUserDto): Promise<User> {
    const user = await this.userModel
      .findOne({ email: email })
      .select('+password verified email dob gender photo bio');

    if (!user) {
      throw new NotFoundException('User not found');
    }

    if (!user.verified) {
      throw new UnauthorizedException(
        `Please <a href="${this.configService.get(
          'RESEND_VERIFY_URL',
        )}">verify</a> your email address to login`,
      );
    }

    // compare passwords
    const areEqual = await this.correctPassword(user.password, password);

    if (!areEqual) {
      throw new UnauthorizedException('Invalid credentials');
    }

    return user;
  }

  async forgotPassword(email: string) {
    const user = await this.userModel.findOne({ email: email, verified: true });

    if (!user) {
      throw new NotFoundException(
        'There is no user with the given email address',
      );
    }

    const token: {
      publicToken: string;
      privateToken: string;
      tokenExpires: Date;
    } = this._createToken();

    user.token = token.privateToken;
    user.tokenExpires = token.tokenExpires;
    await user.save({ validateBeforeSave: false });

    try {
      const resetUrlToken = token.publicToken;

      return { resetUrlToken, email: user.email };
    } catch (error) {
      user.token = undefined;
      user.tokenExpires = undefined;
      await user.save({ validateBeforeSave: false });

      throw new InternalServerErrorException(
        'There was an error, sending the email. Try again later',
      );
    }
  }

  async resetPassword(token: string, password: string) {
    const hashedToken = crypto.createHash('sha256').update(token).digest('hex');

    let user: Partial<any> = {};
    user = await this.userModel.findOne({
      token: hashedToken,
      tokenExpires: { $gt: Date.now() },
    });

    if (!user) {
      throw new BadRequestException(
        'Reset password token is invalid or expired',
      );
    }

    user.password = password;
    user.token = undefined;
    user.tokenExpires = undefined;
    await user.save();

    return user;
  }

  async verifyEmail(token: string) {
    const hashedToken = crypto.createHash('sha256').update(token).digest('hex');

    let user: Partial<any> = {};
    user = await this.userModel.findOne({
      token: hashedToken,
      tokenExpires: { $gt: Date.now() },
    });

    if (!user) {
      throw new BadRequestException('Verification token is invalid or expired');
    }

    user.verified = true;
    user.token = undefined;
    user.tokenExpires = undefined;
    await user.save();

    return user;
  }

  async resendEmail(email: string) {
    const user = await this.userModel.findOne({
      email: email,
      verified: false,
    });

    if (!user) {
      throw new BadRequestException('This user does not exist');
    }

    return this.verificationUrlToken(user);
  }

  async correctPassword(
    candidatePassword: string,
    userPassword: string,
  ): Promise<boolean> {
    return await bcrypt.compare(userPassword, candidatePassword);
  }

  private async verificationUrlToken(user: User) {
    const token: {
      publicToken: string;
      privateToken: string;
      tokenExpires: Date;
    } = this._createToken();

    user.token = token.privateToken;
    user.tokenExpires = token.tokenExpires;
    await user.save({ validateBeforeSave: false });

    const verificationUrlToken = token.publicToken;

    return { name: user.name, email: user.email, verificationUrlToken };
  }

  private _createToken() {
    const publicToken = crypto.randomBytes(32).toString('hex');
    const privateToken = crypto
      .createHash('sha256')
      .update(publicToken)
      .digest('hex');
    const tokenExpires = moment(Date.now())
      .add(this.configService.get('RESET_TOKEN_EXPIRES'), 'hours')
      .toDate();
    return { publicToken, privateToken, tokenExpires };
  }
}
