import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { Gender } from '../../common/gender.enum';
import * as bcrypt from 'bcrypt';
import { Skill } from '../../jobs/types/job.type';

@Schema({
  toObject: { virtuals: true },
  toJSON: { virtuals: true },
  timestamps: true,
})
export class User extends Document {
  @Prop({ required: true })
  name: string;

  @Prop({ required: true, unique: true, lowercase: true })
  email: string;

  @Prop({ required: true, minlength: 8, select: false })
  password: string;

  @Prop()
  bio: string;

  @Prop()
  photo: string;

  @Prop({ type: String, enum: Gender })
  gender: Gender;

  @Prop()
  dob: Date;

  @Prop()
  skills: Skill[];

  @Prop({ select: false, default: null })
  token: string;

  @Prop({ select: false })
  tokenExpires: Date;

  @Prop({ select: false, default: false })
  verified: boolean;
}

const UserSchema = SchemaFactory.createForClass(User);

UserSchema.virtual('photoUrl').get(function () {
  return this.photo && `${process.env.SERVER_UPLOAD_PATH}/${this.photo}`;
});

UserSchema.pre('save', async function (next) {
  if (!this.isModified('password')) {
    return next();
  }

  this.password = await bcrypt.hash(this.password, 12);
  next();
});

export { UserSchema };
