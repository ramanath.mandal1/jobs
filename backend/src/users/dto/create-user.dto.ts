import {
  IsDateString,
  IsEmail,
  IsEnum,
  IsNotEmpty,
  IsOptional,
  IsString,
  Length,
  MinLength,
} from 'class-validator';
import { Gender } from '../../common/gender.enum';

export class CreateUserDto {
  @IsNotEmpty()
  @Length(2)
  name: string;

  @IsEmail()
  email: string;

  @IsNotEmpty()
  @MinLength(8)
  password: string;

  @IsString()
  @IsOptional()
  bio: string;

  @IsOptional()
  photo: any;

  @IsEnum(Gender, { message: 'gender must be Male, Female or Other' })
  @IsOptional()
  gender: Gender;

  @IsOptional()
  @IsDateString()
  dob: string;

  @IsString({ each: true })
  @IsOptional()
  skills: string[];
}
